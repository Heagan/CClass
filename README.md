# CClass

Tired of manually adding a class to your project?

Worry no more, CClass will create them for you with the boilerplate code done and ready for you to expand on!

## What it does

Just run the script file
Creates a .cpp file and a .hpp file
Any imports specified will be added to the header file


