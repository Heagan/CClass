#include <iostream>
#include <string>
#include <fstream>
#include <vector>

using namespace std;

vector<string>	include;
string			className;
ofstream		file;

void createHeader() {
	file << "#ifndef ";
	for (unsigned int i = 0; i < className.length(); i++) {
		if (className[i] < 90 && i > 0) {
			file << "_";
		}
		file << (char)toupper(className[i]);
	}
	file << "_H" << endl;

	file << "# define ";
	for (unsigned int i = 0; i < className.length(); i++) {
		if (className[i] < 90 && i > 0) {
			file << "_";
		}
		file << (char)toupper(className[i]);
	}
	file << "_H" << endl;

	for (unsigned int i = 0; i < include.size(); i++) {
		file << "#include <" + include.at(i) << ">" << endl;
	}
	
}

void createConstr() {
	file << endl;
	file << "class " << className << " {" << endl;
	file << endl;

	file << "public:" << endl;
	file << "\t" << className << "( void );" << endl;
	file << "\t" << className << "( " << className << " &object" << " );" << endl;
	file << "\t~" << className << "( void );" << endl;
	file << "\t" << className << " " << "&operator=( " << className << " const &object );" << endl;

	file << endl;
	file << "};" << endl;
	file << endl;

	file << "#endif\n";
}

void makeCPP() {
	file << "#include \"" << className + ".class.hpp" << "\"" << endl;
	for (unsigned int i = 0; i < include.size(); i++) {
		file << "#include <" + include.at(i) << ">" << endl;
	}

	file << endl;
	file << className << "::" << className << "( void ) {" << endl;
	file << endl << "}" << endl; 

	file << endl;
	file << className << "::" << className << "( " << className << " &object" << " ) {" << endl;
	file << "\t*this = object;" << endl;
	file << "}" << endl;

	file << endl;
	file << className << "::~" << className << "( void ) {" << endl;
	file << endl << "}" << endl; 

	file << endl;
	file << className << "\t&" << className << "::operator=( " << className << " const &object ) {" << endl;
	file << "\treturn *this;" << endl;
	file << "}" << endl; 

	file << endl;
}

void createClass() {
	string		fileName;

	cout << "Enter class name: ";
	getline(cin, fileName);
	className = fileName;
	if (fileName.length() == 0) {
		return ;
	}

	className[0] = (char)toupper(className[0]);
	
	fileName = "./class/" + className + ".class.hpp";
	file.open(fileName.c_str());
	createHeader();
	createConstr();
	file.close();
	
	fileName = "./class/" + className + ".class.cpp";
	file.open(fileName.c_str());
	makeCPP();
	file.close();
	createClass();
}

void	prepare() {
	string line;

	cout << "Include: ";
	getline(cin, line);
	if (line.length() > 0) {
		include.push_back(line);
		prepare();
		return ;
	}

}

int		main() {
	prepare();
	createClass();

	return 0;
}