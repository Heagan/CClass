FILE = main.cpp

#WIN =  -static-libgcc -static-libstdc++

ERROR = -Wall -Wextra -Werror

NAME = CClass

all:
	g++ -o $(NAME) $(FILE) $(WIN) $(ERROR)

